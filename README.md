## Benchmark uprobe overhead

### Usage

To run the benchmark after installing dependencies, run the script with no arguments:

```
$ benchmark_uprobe.sh
```

It outputs verbose results to both standard out and to a logfile: `benchmark_uprobe.log`

The last section of output is a summary report showing the total CPU time and the uprobe overhead
during each run of each test case.

### Purpose

This benchmark attempts to measure the CPU overhead added by the uprobe instrumentation
during each call to an instrumented function.  It exercises the following common use cases:
* No uprobe or disabled uprobe (baseline, no overhead)
* Counter (perf-stat and BPF)
* Latency measurement of function call (BPF)
* Stack trace (perf-record using framepointers and DWARF)

### Requirements

This benchmark assumes you have installed the following:
* perf - Linux perf tools.
  If not already installed, install the `linux-tools-common` package to get `perf` itself. Then run `perf` to get the name of the
  kernel-specific package to install (e.g. `linux-tools-$( uname -r )`).
* BCC - [BPF Compiler Collection](https://www.iovisor.org/technology/bcc)
  * BCC can be installed as package `bcc-tools` (from iovisor) or `bpfcc-tools` (from Ubuntu), or a newer version can be built from source.
  * Before running this benchmark script, ensure your PATH environment variable includes the BCC tools dir, typically:
    * `/usr/share/bcc/tools` if installed using the iovisor package or building from source
    * `/usr/sbin` when installed using the Ubuntu package
  * See the BCC install docs:
    * [binary packages](https://github.com/iovisor/bcc/blob/master/INSTALL.md#ubuntu---binary)
    * [building from source](https://github.com/iovisor/bcc/blob/master/INSTALL.md#ubuntu---source)
  * Note: If you build BCC from source and use a non-default base path for installing it (i.e. not "/usr"), then the benchmark script will
    automatically infer the `PYTHONPATH` and `LD_LIBRARY_PATH` based on the path to your `funccount` BCC tool.
* gcc - Required to build the C program to observe.
  If preferred, you may skip this step and instead build the binary elsewhere and copy it into place.

On Ubuntu, the following should get you close:

```
$ sudo apt-get install linux-tools-common linux-tools-$( uname -r )
$ sudo apt-get install bpfcc-tools
$ sudo apt-get install gcc
```

### Implementation overview

This benchmark builds and runs a small C program (`benchmark_uprobe.c`) as the benchmark target to observe.
That C program takes 1 or 2 arguments:
1. Number of times to call function `myfunc`, which is the function we will instrument with a uprobe.
2. (Optional) baseline expected CPU time per call to `myfunc`, measured in fractional microseconds (e.g. 0.195 us).

As the last step, this program reports its rusage metrics (see the `getrusage` syscall manpage).
If the program was called with its 2nd argument (expected baseline cost per function call), then it also
reports the overhead above that baseline (total CPU usage minus the baseline).

The output of each run is verbose, and it concludes with a summary report
showing each test case's CPU time and overhead during each run.

By default, each test case is run 3 times, and each run makes 10^6 calls to `myfunc`.
A larger number of calls per run produces more stable, consistent measurements among runs.

The intent here is to measure per-call overhead so we can estimate the impact of
observing all calls to a certain function.  Instrumenting all calls to a frequently called function
can potentially severely degrade performance, so knowing the overhead per call is useful.
