#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <string.h>

extern int errno;

void myfunc(int n)
{
    int i;
    static unsigned int x = 0;  // x will periodically wrap back to zero after reaching max integer value.
    for (i = 0; i < n; i++) {
        x += 1;
    }
}

void show_rusage_summary(int num_calls_to_function, double baseline_cpu_us_per_call)
{
    struct rusage usage;
    double total_cpu_time_us;
    double user_cpu_time_us;
    double system_cpu_time_us;
    double overhead_total_cpu_us_per_call;
    double overhead_user_cpu_us_per_call;
    double overhead_system_cpu_us_per_call;

    if (getrusage(RUSAGE_SELF, &usage) != 0) {
        printf("Failed to get rusage metrics.  Error (%d): %s\n", errno, strerror(errno));
        exit(1);
    }

    user_cpu_time_us = usage.ru_utime.tv_sec * 1000000 + usage.ru_utime.tv_usec;
    system_cpu_time_us = usage.ru_stime.tv_sec * 1000000 + usage.ru_stime.tv_usec;
    total_cpu_time_us = user_cpu_time_us + system_cpu_time_us;

    printf("Resource usage metrics:\n");
    printf("  Total CPU time:    %.6f seconds\n", total_cpu_time_us / 1000000);
    printf("  User CPU time:     %.6f seconds (%0.0f%% of total)\n", user_cpu_time_us / 1000000, 100 * user_cpu_time_us / total_cpu_time_us);
    printf("  System CPU time:   %.6f seconds (%0.0f%% of total)\n", system_cpu_time_us / 1000000, 100 * system_cpu_time_us / total_cpu_time_us);
    printf("  Context switches:  %lu (%lu voluntary + %lu involuntary)\n",
        usage.ru_nvcsw + usage.ru_nvcsw,
        usage.ru_nvcsw,
        usage.ru_nvcsw
    );
    printf("  CPU time per call: %.3f us/call\n", total_cpu_time_us / num_calls_to_function);
    if (baseline_cpu_us_per_call > 0) {
        // This benchmark is expected to cost 100% user time when no instrumentation is present, so we attribute the whole baseline to user time.
        overhead_total_cpu_us_per_call = ( total_cpu_time_us / num_calls_to_function ) - baseline_cpu_us_per_call;
        overhead_user_cpu_us_per_call = ( user_cpu_time_us / num_calls_to_function ) - baseline_cpu_us_per_call;
        overhead_system_cpu_us_per_call = ( system_cpu_time_us / num_calls_to_function ) - 0;
        if (overhead_total_cpu_us_per_call >= 0.001 && overhead_user_cpu_us_per_call > 0) {
            printf("  Overhead per call: %.3f us/call (%0.0f%% user, %0.0f%% system)\n",
                overhead_total_cpu_us_per_call,
                100 * overhead_user_cpu_us_per_call / overhead_total_cpu_us_per_call,
                100 * overhead_system_cpu_us_per_call / overhead_total_cpu_us_per_call
            );
        }
        else {
            printf("  Overhead per call: none (CPU usage did not exceed the given baseline)\n");
        }
    }
}

int main (int argc, char *argv[])
{
    int i;
    long unsigned int num_calls_to_function;
    double baseline_cpu_us_per_call = 0;

    if (argc < 2 || argc > 3) {
        printf("Usage: %s num_calls_to_function [baseline_cpu_us_per_call] \n", argv[0]);
        exit(1);
    }
    num_calls_to_function = atol(argv[1]);
    if (argc == 3)
        baseline_cpu_us_per_call = atof(argv[2]);

    printf("Starting workload: %lu calls to function \"myfunc\".\n", num_calls_to_function);
    for (i = 0; i < num_calls_to_function; i++) {
        myfunc(100);
    }
    printf("Finished workload.\n");

    show_rusage_summary(num_calls_to_function, baseline_cpu_us_per_call);
}
