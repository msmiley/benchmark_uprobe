This directory contains slightly modified versions of a few version 0.10 BCC tools.
* funccount - Uses a uprobe to add a counter at the entry of a specified userspace function.
* funclatency - Uses a uprobe and uretprobe to capture the timestamp of entry and return from a function.

On some kernels, the BPF C code throws warnings during compilation due to newer kernel versions
introducing macro `asm_inline`, which libbcc versions <= 0.10 cannot handle.
We work around this compatibility issue by redefining `asm_inline` to be `asm`.
Patch files are included here for reference.  Other similar version incompatibilities can also
cause similar warnings, which are often noisy but not practically harmful to the outcome.

These modified BCC python scripts still depend on the rest of the BCC suite being installed.

By default, these modified versions of the BCC scripts will not be used by the benchmark.
If you want to use them (or any other custom build of BCC), add that directory to your
PATH environment variable before running the benchmark suite.

Example:

```
$ export PATH=./bcc_tools:$PATH
$ ./benchmark_uprobe.sh
```

The [BCC suite](https://github.com/iovisor/bcc/blob/master/LICENSE.txt) is licensed under Apache License 2.0:
https://www.apache.org/licenses/LICENSE-2.0.html
The contents of this directory are a derivative work under that license, and a copy of that license is included.
