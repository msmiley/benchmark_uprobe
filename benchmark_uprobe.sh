#!/bin/bash

set -o pipefail
set -o errtrace
set -o errexit

NUM_RUNS_PER_TEST_CASE=3
NUM_FUNC_CALLS_PER_RUN=1000000
EXECUTABLE_TO_RUN="/tmp/benchmark_uprobe"
BASE_DIR="$( dirname $0 )"
LOG_FILE="benchmark_uprobe.log"
TEST_CASE_ID=0

function main()
{
    (
        resolve_dependencies
        show_reference_info
        initial_setup
        test_case_no_uprobe
        test_case_disabled_uprobe
        test_case_counter_perf_stat
        test_case_counter_bpf_funccount
        test_case_latency_bpf_funclatency
        test_case_stack_trace_perf_fp
        test_case_stack_trace_perf_dwarf
    ) 2>&1 | tee "$LOG_FILE"

    (
        show_results_summary
    ) 2>&1 | tee -a "$LOG_FILE"
}

function resolve_dependencies()
{
    [[ $( uname -s ) == "Linux" ]] || die "This benchmark only runs on Linux.  It measures Linux uprobe overhead in several common use cases."

    EXEC_FUNCCOUNT=$( command -v funccount || command -v funccount-bpfcc || true )
    [[ -z "$EXEC_FUNCCOUNT" ]] && die "Cannot find funccount.  Is BCC installed and in this shell's PATH?  Try: PATH=\$PATH:/usr/share/bcc/tools"

    EXEC_FUNCLATENCY=$( command -v funclatency || command -v funclatency-bpfcc || true )
    [[ -z "$EXEC_FUNCLATENCY" ]] && die "Cannot find funclatency.  Is BCC installed and in this shell's PATH?"

    add_extra_envs_if_using_custom_bcc_path

    command -v perf > /dev/null || die "Cannot find perf.  Is linux-tools installed?"
}

function add_extra_envs_if_using_custom_bcc_path()
{
    # If BCC was built from source and installed to a non-default base path (e.g. "/usr/local" instead of "/usr"), then
    # for the BCC python scripts to resolve their dependencies, we must set PYTHONPATH and LD_LIBRARY_PATH.
    #
    # On some platforms, the "sudo" security policy strips LD_LIBRARY_PATH from the environment before running the program
    # as a safety precaution.  Here we do need that custom library path, so we explicitly set it on the command line to be
    # run via sudo.

    if [[ "$EXEC_FUNCCOUNT" =~ '/share/bcc/tools/funccount' ]] ; then
      BCC_INSTALL_BASE_PATH="${EXEC_FUNCCOUNT%/share/bcc/tools/funccount}"
      if [[ "$BCC_INSTALL_BASE_PATH" != "/usr" ]] ; then
          EXTRA_BCC_ENVS+=" LD_LIBRARY_PATH=$BCC_INSTALL_BASE_PATH/lib"
          EXTRA_BCC_ENVS+=" PYTHONPATH=$BCC_INSTALL_BASE_PATH/lib/python3/dist-packages"
          EXEC_FUNCCOUNT="$EXTRA_BCC_ENVS $EXEC_FUNCCOUNT"
          EXEC_FUNCLATENCY="$EXTRA_BCC_ENVS $EXEC_FUNCLATENCY"
      fi
    fi
}

function show_reference_info()
{
    echo "Log file: $LOG_FILE"
    echo "Version info:"
    echo "  Kernel version: $( uname -r )"
    echo "  Perf version:   $( perf version )"
    echo "  BCC version:    $( try_to_infer_bcc_version )"
    echo "  OS release:     $( lsb_release --description )"
    echo "  Benchmark SHA1: $( git --git-dir="$BASE_DIR/.git" log -1 --pretty='%H' 2> /dev/null || echo 'Unknown' )"
    echo "Hardware:"
    echo "  CPU model:   $( cat /proc/cpuinfo | grep -m1 'model name' )"
    echo "  CPU sockets: $( cat /proc/cpuinfo | grep 'physical id' | sort -u | wc -l )"
    echo "  CPU cores:   $( cat /proc/cpuinfo | grep 'core id' | sort -u | wc -l )"
    echo "  CPU threads: $( cat /proc/cpuinfo | grep 'processor' | wc -l )"
    echo "BCC tools:"
    echo "  $EXEC_FUNCCOUNT"
    echo "  $EXEC_FUNCLATENCY"
    echo
}

function try_to_infer_bcc_version()
{
    (
        python3 -c 'import bcc; print(bcc.__version__)' \
        || python -c 'import bcc; print(bcc.__version__)' \
        || dpkg-query --show bpfcc-tools bcc-tools | grep -m 1 '[0-9]' \
        || echo 'Unknown'
    ) 2> /dev/null
}

function initial_setup()
{
    build_binary
    clean_up_residual_uprobe_if_exists
    measure_baseline
    echo
}

function build_binary()
{
    echo "Building binary."
    CFLAGS+=" -fno-inline"
    CFLAGS+=" -fno-omit-frame-pointer"
    gcc -o "$EXECUTABLE_TO_RUN" "$BASE_DIR/benchmark_uprobe.c"
}

function clean_up_residual_uprobe_if_exists()
{
    if [[ $( sudo perf probe --list benchmark:* 2>&1 | wc -l ) -gt 0 ]] ; then
        delete_uprobe_for_perf
    fi
}

function measure_baseline()
{
    # Measure the unintrumented duration several times, and keep the fastest one as the baseline (principle of least purturbation).
    # Add that baseline measurement as a configuration input to the toy program so it can subtract it in future runs to compute overhead.

    COMMAND_TO_BENCHMARK="taskset --cpu-list 0 $EXECUTABLE_TO_RUN $NUM_FUNC_CALLS_PER_RUN"
    echo "Measuring baseline."
    BASELINE_CPU_TIME_PER_CALL_US=$(
      for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
      do
          $COMMAND_TO_BENCHMARK
      done | grep 'CPU time per call:' | awk '{ print $5 }' | sort -n | head -n1
    )
    echo "Baseline: $BASELINE_CPU_TIME_PER_CALL_US us/call"
    COMMAND_TO_BENCHMARK+=" $BASELINE_CPU_TIME_PER_CALL_US"
}

function test_case_no_uprobe()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark without uprobe."
    echo "Notes: Baseline CPU time for the toy program.  Expect 100% user, 0% system.  We subtract it from other benchmarks to measure overhead."
    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: No uprobe, Run: $RUN_ID:"
        $COMMAND_TO_BENCHMARK
    done
    echo
}

function test_case_disabled_uprobe()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark with uprobe created but disabled."
    echo "Notes: Identical to the baseline of having no uprobe.  A disabled uprobe does not change the execution path for the process running the binary."
    create_uprobe_for_perf
    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: Disabled uprobe, Run: $RUN_ID:"
        $COMMAND_TO_BENCHMARK
    done
    delete_uprobe_for_perf
    echo
}

function test_case_counter_perf_stat()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark using uprobe for incrementing a counter via perf-stat."
    echo "Notes: Counters are the cheapest type of instrumentation."
    create_uprobe_for_perf
    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: uprobe for counter (perf stat), Run: $RUN_ID:"
        sudo perf stat -e benchmark:myfunc -- $COMMAND_TO_BENCHMARK
    done
    delete_uprobe_for_perf
    echo
}

function test_case_counter_bpf_funccount()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark using uprobe for incrementing a counter via BPF."
    echo "Notes: Comparable to perf-stat."

    # To capture all events, we overestimate a mean of 20 us per call.
    SECONDS_TO_RUN_BPF_INSTRUMENTATION=$(( $NUM_FUNC_CALLS_PER_RUN * 20 / 1000000 ))

    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: uprobe for counter (BPF), Run: $RUN_ID:"
        sudo $EXEC_FUNCCOUNT --duration=$SECONDS_TO_RUN_BPF_INSTRUMENTATION "$EXECUTABLE_TO_RUN:myfunc" &
        COUNTER_PID=$!
        # Wait a moment for the uprobe instrumentation to be injected before starting the workload.
        sleep 1
        $COMMAND_TO_BENCHMARK
        # Notes:
        # Running "funccount" without a duration arg makes it wait forever.
        # Sending it SIGINT does not match its KeyboardInterrupt exception handler.
        # Sending it SIGTERM aborts it without showing output.  That would be ok in this special case
        # because we just want to measure its overhead not the results.  But instead let's just be patient
        # and wait for the duration to elapse so we can confirm that the instrumentation did its job correctly.
        # That does mean we have to guess at a reasonable duration.  5s is more than enough to cover 1M calls.
        #sudo kill -SIGINT $COUNTER_PID
        wait
    done
    echo
}

function test_case_latency_bpf_funclatency()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark using pair of uprobe/uretprobe for measuring function latency via BPF."
    echo "Notes: Duration measurements use a uprobe and a uretprobe to capture a pair of timestamps.  More overhead than an counter."

    # To capture all events, we overestimate a mean of 20 us per call.
    SECONDS_TO_RUN_BPF_INSTRUMENTATION=$(( $NUM_FUNC_CALLS_PER_RUN * 20 / 1000000 ))

    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: uprobe/uretprobe for latency (BPF), Run: $RUN_ID:"
        sudo $EXEC_FUNCLATENCY --duration=$SECONDS_TO_RUN_BPF_INSTRUMENTATION "$EXECUTABLE_TO_RUN:myfunc" &
        COUNTER_PID=$!
        # Wait a moment for the uprobe instrumentation to be injected before starting the workload.
        sleep 1
        $COMMAND_TO_BENCHMARK
        wait
    done
    echo
}

function test_case_stack_trace_perf_fp()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark using uprobe for stack traces via perf-record using frame pointers."
    echo "Notes: This uses 1 uprobe but captures a stack trace.  FP is cheaper than DWARF for both CPU time and sample size."
    create_uprobe_for_perf
    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: uprobe for stack traces (fp), Run: $RUN_ID:"
        sudo perf record -g --call-graph=fp -e benchmark:myfunc -o /dev/null -- $COMMAND_TO_BENCHMARK
    done
    delete_uprobe_for_perf
    echo
}

function test_case_stack_trace_perf_dwarf()
{
    (( TEST_CASE_ID += 1 ))
    echo "Test case $TEST_CASE_ID: Running benchmark using uprobe for stack traces via perf-record using DWARF."
    echo "Notes: This uses 1 uprobe but captures a stack trace.  FP is cheaper than DWARF for both CPU time and sample size.  DWARF must copy the whole 8 KB stack."
    create_uprobe_for_perf
    for RUN_ID in $( seq $NUM_RUNS_PER_TEST_CASE )
    do
        echo
        echo "Case $TEST_CASE_ID: uprobe for stack traces (dwarf), Run: $RUN_ID:"
        sudo perf record -g --call-graph=dwarf -e benchmark:myfunc -o /dev/null -- $COMMAND_TO_BENCHMARK
    done
    delete_uprobe_for_perf
    echo
}

function show_results_summary()
{
    echo "Results summary:"

    echo
    echo "Mean total CPU time per call:"
    cat "$LOG_FILE" | grep -e '^Test case' -e '^Notes:' -e '^ *CPU time per call' | grep -B3 'CPU time per call'

    echo
    echo "Mean overhead CPU time per call (total CPU time - baseline):"
    cat "$LOG_FILE" | grep -e '^Test case' -e '^Notes:' -e '^ *Overhead per call' | grep -B3 'Overhead per call'
}

function create_uprobe_for_perf()
{
    echo "Creating uprobe."
    sudo perf probe -q -x $EXECUTABLE_TO_RUN --add benchmark:myfunc=myfunc
    sudo perf probe --list benchmark:*
}

function delete_uprobe_for_perf()
{
    echo "Removing uprobe."
    sudo perf probe --del benchmark:myfunc
}

function die()
{
    echo "ERROR: $@"
    exit 1
}

main "$@"
